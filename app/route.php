<?php
// +---------------------------------------------------------------------+
// | OneBase    | [ WE CAN DO IT JUST THINK ]                            |
// +---------------------------------------------------------------------+
// | Licensed   | http://www.apache.org/licenses/LICENSE-2.0 )           |
// +---------------------------------------------------------------------+
// | Author     | Bigotry <3162875@qq.com>                               |
// +---------------------------------------------------------------------+
// | Repository | https://gitee.com/Bigotry/OneBase                      |
// +---------------------------------------------------------------------+

/**
 * 路由文件
 */

return [
    'index'                 => 'index/index',
    'game_list'             => 'game/index',
    'game_list/:cid'        => 'game/index',
    'gift_list'             => 'gift/index',
    'gift_list/:gid'        => 'gift/index',
    'article'               => 'article/index',
    'center'                => 'center/index',
    'pay'                   => 'pay/index',
    'login'                 => 'login/login',
    'loginHandle'           => 'login/loginHandle',
    'register'              => 'login/register',
    'registerHandle'        => 'login/registerHandle',
    'logout'                => 'login/logout',
];
