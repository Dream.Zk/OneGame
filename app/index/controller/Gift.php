<?php
// +---------------------------------------------------------------------+
// | OneBase    | [ WE CAN DO IT JUST THINK ]                            |
// +---------------------------------------------------------------------+
// | Licensed   | http://www.apache.org/licenses/LICENSE-2.0 )           |
// +---------------------------------------------------------------------+
// | Author     | Bigotry <3162875@qq.com>                               |
// +---------------------------------------------------------------------+
// | Repository | https://gitee.com/Bigotry/OneBase                      |
// +---------------------------------------------------------------------+

namespace app\index\controller;

/**
 * 礼包中心控制器
 */
class Gift extends IndexBase
{
    
    // 礼包中心首页
    public function index()
    {
        
        set_url();
        
        $this->assign('data', $this->logicGift->getGiftData($this->param));
        
        return $this->fetch('index');
    }
}
