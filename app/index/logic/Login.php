<?php
// +---------------------------------------------------------------------+
// | OneBase    | [ WE CAN DO IT JUST THINK ]                            |
// +---------------------------------------------------------------------+
// | Licensed   | http://www.apache.org/licenses/LICENSE-2.0 )           |
// +---------------------------------------------------------------------+
// | Author     | Bigotry <3162875@qq.com>                               |
// +---------------------------------------------------------------------+
// | Repository | https://gitee.com/Bigotry/OneBase                      |
// +---------------------------------------------------------------------+

namespace app\index\logic;

use think\helper\Hash;

/**
 * 登录注册业务逻辑
 */
class Login extends IndexBase
{
    
    /**
     * 登录处理
     */
    public function loginHandle($param = [])
    {
        
        $username = (string)$param['username']; $password = (string)$param['password'];
        
        $validate_result = $this->validateLogin->scene('index')->batch()->check(compact('username','password'));
        
        // 验证输入
        if (!$validate_result) {  return [RESULT_ERROR, $this->validateLogin->getError()]; }
        
        // 查找会员
        $member = $this->modelMember->getInfo(['username' => $username]);
        
        if (empty($member)) { return [RESULT_ERROR, '账号不存在或被禁用']; }
        
        // 旧版系统与新版系统密码验证
        if (!((empty($member['password_version']) && Hash::check($password, $member['password'])) || (DATA_NORMAL == $member['password_version'] && data_md5_key($password) == $member['password']))) {
            
            return [RESULT_ERROR, '登录密码错误'];
        }
        
        $update_data[TIME_UT_NAME]  = TIME_NOW;
        $update_data['ip']          = request()->ip();
        
        if (empty($member['password_version'])) {
            
            $update_data['password_version']    = DATA_NORMAL;
            $update_data['password']            = $password;
        }
        
        $this->modelMember->updateInfo(['id' => $member['id']], $update_data);

        $this->login($member);
        
        return [RESULT_SUCCESS, '登录成功', get_url(url('index/index'))];
    }
    
    /**
     * 注册处理
     */
    public function registerHandle($param = [])
    {
        
        $validate_result = $this->validateRegister->scene('index')->batch()->check($param);
        
        // 验证输入
        if (!$validate_result) {  return [RESULT_ERROR, $this->validateRegister->getError()]; }
        
        // 查找会员
        $member = $this->modelMember->getInfo(['username' => $param['username']]);
        
        if (!empty($member)) { return [RESULT_ERROR, '此账号已存在']; }
        
        $param[TIME_UT_NAME]        = TIME_NOW;
        $param['ip']                = request()->ip();
        $param['password_version']  = DATA_NORMAL;
        $param['nickname']          = $param['username'];
        
        $result = $this->modelMember->setInfo($param);

        if (!$result) { return [RESULT_ERROR, '注册失败，请稍后再试']; }
        
        $this->login($member);
        
        return [RESULT_SUCCESS, '注册成功', get_url(url('index/index'))];
    }
    
    /**
     * 登录
     */
    public function login($member = [])
    {
        
        $auth = ['member_id' => $member['id'], TIME_UT_NAME => TIME_NOW];

        session('member_info', $member);
        session('member_auth', $auth);
        session('member_auth_sign', data_auth_sign($auth));
    }
    
    /**
     * 退出
     */
    public function logout()
    {
        
        session('member_info', null);
        session('member_auth', null);
        session('member_auth_sign', null);
    }
}
