<?php
// +---------------------------------------------------------------------+
// | OneBase    | [ WE CAN DO IT JUST THINK ]                            |
// +---------------------------------------------------------------------+
// | Licensed   | http://www.apache.org/licenses/LICENSE-2.0 )           |
// +---------------------------------------------------------------------+
// | Author     | Bigotry <3162875@qq.com>                               |
// +---------------------------------------------------------------------+
// | Repository | https://gitee.com/Bigotry/OneBase                      |
// +---------------------------------------------------------------------+

namespace app\index\logic;

/**
 * 礼包中心逻辑
 */
class Gift extends IndexBase
{
    
    /**
     * 获取首页数据
     */
    public function getGiftData($param = [])
    {
        
        // 游戏列表
        $data['game_list'] = $this->getGameList();
        
        // 礼包列表
        $data['gift_list'] = $this->getGiftList($param);

        return $data;
    }
    
    /**
     * 游戏列表
     */
    public function getGameList()
    {
        
        $where[DATA_STATUS_NAME]      = ['neq', DATA_DELETE];
        
        return $this->modelWgGame->getList($where, 'id,game_name', 'sort desc,create_time desc', false);
    }
    
    /**
     * 礼包列表
     */
    public function getGiftList($param = [])
    {
        
        $this->modelWgGift->alias('gi');
        
        $join = [ [SYS_DB_PREFIX . 'wg_game ga', 'ga.id = gi.game_id'] ];
        
        $where['gi.' . DATA_STATUS_NAME] = ['neq', DATA_DELETE];
        
        $field = 'gi.id,gi.gift_name,gi.gift_describe,gi.create_time,ga.game_logo,ga.game_head,ga.game_code';
        
        !empty($param['gid'])         && $where['gi.game_id']   = (int)$param['gid'];
        !empty($param['keyword'])     && $where['gi.gift_name']    = ['like','%'.(string)$param['keyword'].'%'];
        
        $list = $this->modelWgGift->getList($where, $field, 'gi.create_time desc', 5, $join);
        
        $list_ids = array_extract($list);
        
        $key_where['is_get']   = DATA_DISABLE;
        $key_where['gift_id']  = ['in', $list_ids];
        
        $list_number = $this->modelWgGiftKey->where($key_where)->group("gift_id")->field("gift_id,count('id') as number")->select();
        
        foreach ($list as &$info)
        {
            foreach ($list_number as $number_info)
            {
                $info['id'] != $number_info['gift_id'] ?: $info['number'] = $number_info['number'];
            }
        }
        
        return $list;
    }
    
}
