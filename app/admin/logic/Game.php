<?php
// +---------------------------------------------------------------------+
// | OneBase    | [ WE CAN DO IT JUST THINK ]                            |
// +---------------------------------------------------------------------+
// | Licensed   | http://www.apache.org/licenses/LICENSE-2.0 )           |
// +---------------------------------------------------------------------+
// | Author     | Bigotry <3162875@qq.com>                               |
// +---------------------------------------------------------------------+
// | Repository | https://gitee.com/Bigotry/OneBase                      |
// +---------------------------------------------------------------------+

namespace app\admin\logic;

/**
 * 游戏信息相关逻辑
 */
class Game extends AdminBase
{
    
    /**
     * 游戏分类列表
     */
    public function getCategoryList($where = [], $field = true, $order = '', $paginate = 0)
    {
        
        return $this->modelWgCategory->getList($where, $field, $order, $paginate);
    }
    
    /**
     * 获取分类信息
     */
    public function getCategoryInfo($where = [], $field = true)
    {
        
        return $this->modelWgCategory->getInfo($where, $field);
    }
    
    /**
     * 分类信息编辑
     */
    public function categoryEdit($data = [])
    {
        
        $validate_result = $this->validateGameCategory->scene('edit')->check($data);
        
        if (!$validate_result) : return [RESULT_ERROR, $this->validateGameCategory->getError()]; endif;
        
        $result = $this->modelWgCategory->setInfo($data);
        
        $handle_text = empty($data['id']) ? '新增' : '编辑';
        
        $result && action_log($handle_text, '游戏分类' . $handle_text . '，category_name：' . $data['category_name']);
        
        return $result ? [RESULT_SUCCESS, '操作成功', url('categoryList')] : [RESULT_ERROR, $this->modelWgCategory->getError()];
    }
    
    /**
     * 分类信息删除
     */
    public function categoryDel($where = [])
    {
        
        $result = $this->modelWgCategory->deleteInfo($where);
        
        $result && action_log('删除', '游戏分类删除' . '，where：' . http_build_query($where));
        
        return $result ? [RESULT_SUCCESS, '删除成功'] : [RESULT_ERROR, $this->modelWgCategory->getError()];
    }
    
    /**
     * 游戏列表
     */
    public function getGameList($where = [], $field = 'g.*,c.category_name', $order = 'g.create_time desc', $paginate = 0)
    {
        
        $this->modelWgGame->alias('g');
        
        $join = [
                    [SYS_DB_PREFIX . 'wg_category c', 'c.id = g.game_category_id'],
                ];
        
        $where['g.' . DATA_STATUS_NAME] = ['neq', DATA_DELETE];
        
        return $this->modelWgGame->getList($where, $field, $order, $paginate, $join);
    }
    
    /**
     * 获取游戏信息
     */
    public function getGameInfo($where = [], $field = true)
    {
        
        $info = $this->modelWgGame->getInfo($where, $field);
        
        !empty($info['website_intro_imgs']) && $info['website_intro_imgs_array'] = str2arr($info['website_intro_imgs']);
        !empty($info['website_screenshot']) && $info['website_screenshot_array'] = str2arr($info['website_screenshot']);
        !empty($info['maintain_end_time'])  ?  $info['maintain_end_time']        = format_time($info['maintain_end_time'], 'Y-m-d H:i') : $info['maintain_end_time'] = '';
        
        return $info;
    }
    
    /**
     * 游戏信息编辑
     */
    public function gameEdit($data = [])
    {
        
        $validate_result = $this->validateGame->scene('edit')->check($data);
        
        if (!$validate_result) : return [RESULT_ERROR, $this->validateGame->getError()]; endif;
        
        !empty($data['maintain_end_time']) && $data['maintain_end_time'] = strtotime($data['maintain_end_time']);
        
        $result = $this->modelWgGame->setInfo($data);
        
        $handle_text = empty($data['id']) ? '新增' : '编辑';
        
        $result && action_log($handle_text, '游戏信息' . $handle_text . '，id：' . $data['id']);
        
        return $result ? [RESULT_SUCCESS, '操作成功', url('gameList')] : [RESULT_ERROR, $this->modelWgGame->getError()];
    }
    
    /**
     * 游戏删除
     */
    public function gameDel($where = [])
    {
        
        $result = $this->modelWgGame->deleteInfo($where);
        
        $result && action_log('删除', '游戏信息删除' . '，where：' . http_build_query($where));
        
        return $result ? [RESULT_SUCCESS, '删除成功'] : [RESULT_ERROR, $this->modelWgGame->getError()];
    }
    
    
    /**
     * 获取区服列表
     */
    public function getServerList($where = [], $field = 's.*,g.game_name', $order = 's.create_time desc', $paginate = 0)
    {
        
        
        $this->modelWgServer->alias('s');
        
        $join = [
                    [SYS_DB_PREFIX . 'wg_game g', 's.game_id = g.id'],
                ];
        
        $where['s.' . DATA_STATUS_NAME] = ['neq', DATA_DELETE];
        
        return $this->modelWgServer->getList($where, $field, $order, $paginate, $join);
    }
    
    /**
     * 获取区服信息
     */
    public function getServerInfo($where = [], $field = true)
    {
        
        $info = $this->modelWgServer->getInfo($where, $field);
        
        !empty($info['maintain_end_time'])  ?  $info['maintain_end_time']   = format_time($info['maintain_end_time'], 'Y-m-d H:i')  : $info['maintain_end_time']    = '';
        !empty($info['start_time'])         ?  $info['start_time']          = format_time($info['start_time'], 'Y-m-d H:i')         : $info['start_time']           = '';
        
        return $info;
    }
    
    /**
     * 区服信息编辑
     */
    public function serverEdit($data = [])
    {
        
        $validate_result = $this->validateServer->scene('edit')->check($data);
        
        if (!$validate_result) : return [RESULT_ERROR, $this->validateServer->getError()]; endif;
        
        !empty($data['maintain_end_time'])  && $data['maintain_end_time']   = strtotime($data['maintain_end_time']);
        !empty($data['start_time'])         && $data['start_time']          = strtotime($data['start_time']);
        
        $result = $this->modelWgServer->setInfo($data);
        
        $handle_text = empty($data['id']) ? '新增' : '编辑';
        
        $result && action_log($handle_text, '游戏区服' . $handle_text . '，server_name：' . $data['server_name']);
        
        return $result ? [RESULT_SUCCESS, '操作成功', url('serverList')] : [RESULT_ERROR, $this->modelWgServer->getError()];
    }
    
    /**
     * 区服信息删除
     */
    public function serverDel($where = [])
    {
        
        $result = $this->modelWgServer->deleteInfo($where);
        
        $result && action_log('删除', '区服删除' . '，where：' . http_build_query($where));
        
        return $result ? [RESULT_SUCCESS, '删除成功'] : [RESULT_ERROR, $this->modelWgServer->getError()];
    }
}
